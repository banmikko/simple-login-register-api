import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Response from 'App/Helpers/Response'
import User from 'App/Models/User'
import AuthValidator from 'App/Validators/AuthValidator'

export default class AuthenticationController {
    public async register({request,response}: HttpContextContract){
        await request.validate(AuthValidator)
        const apiResponse = new Response(response)
        const data = request.only([
            "email",
            "username",
            "password"
            
        ])

        const emailChecker = await User.query()
        .where('email', data.email)
        .first()

        if(!emailChecker){
            await User.create({
                email:data.email,
                username: data.username,
                password: data.password
            })
            return apiResponse.data("Account has been created, welcome " + data.username)  

        } else {
            return apiResponse.badRequest("Email already taken, try another one ")
        }   
    }

    public async login({auth,request,response}: HttpContextContract){
        const apiResponse = new Response(response)
        const data = request.only([
            "email",
            "password"
        ])

        //Authenticate User!
        try {
            const authentication = await auth.use('api').attempt(data.email,data.password)
            const user = await User.query()
            .where("id", authentication.user.id)
            .first()


            if (!user) {
                return apiResponse.notFound(
                  "No account found, please sign up "
                );
              }

              return apiResponse.data("Logged in Succesfully! " + data.email)
        
        } catch(error){
            //Error to check if Input is Incorrect
            const emailNotFoundMsg = "E_INVALID_AUTH_UID: No user found"
            const passwordMisMatchMsg = "E_INVALID_AUTH_PASSWORD: Incorrect password"

            if (error.message == passwordMisMatchMsg) {
                return apiResponse.badRequest("Incorrect Password.")
              }
        
              if (error.message == emailNotFoundMsg) {
                return apiResponse.badRequest("No account found, please sign up")
              }
        
              console.log(error)
              return apiResponse.badRequest("Invalid credentials")
            }
        }
    }

